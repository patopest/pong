# Pong
Super Simple pong game made as a test of the Kivy GUI package for Python.  
Based on this [tutorial](https://kivy.org/doc/stable/tutorials/pong.html).  

Packaged as standalone MacOs .app using `py2app`.  